import Home from "./assets/pages/Home";

function App() {
	return (
		<div className="app">
			<Home />
		</div>
	);
}

export default App;
