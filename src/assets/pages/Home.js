import React, { Component } from "react";
import { IoAdd, IoAirplaneOutline, IoFastFoodOutline } from "react-icons/io5";
import { MdDelete } from "react-icons/md";
import { BiHomeHeart } from "react-icons/bi";
import toast, { Toaster } from "react-hot-toast";
import "../styles/home.css";

export default class Home extends Component {
	state = {
		category: "",
		title: "",
		price: "",
		expense: [],
		filterExp: "all",
	};
	totalExp = 0;

	handleCatEmptyInput = () => {
		toast.error("Please Select a Category!");
	};
	handleTitleEmptyInput = () => {
		toast.error("Expense Details Can't be Empty!");
	};
	handleAmtEmptyInput = () => {
		toast.error("Amount Can't be Empty!");
	};

	handleCategory = (e) => {
		this.setState({
			category: e.target.value,
		});
	};
	filterCategory = (e) => {
		this.setState({
			filterExp: e.target.value,
		});
	};

	handleTitle = (e) => {
		this.setState({
			title: e.target.value,
		});
	};

	handlePrice = (e) => {
		this.setState({
			price: e.target.value,
		});
	};

	submitHandle = (e) => {
		e.preventDefault();

		if (this.state.category === "") {
			this.handleCatEmptyInput();
		} else if (this.state.title.trim() === "") {
			this.handleTitleEmptyInput();
		} else if (this.state.price === "") {
			this.handleAmtEmptyInput();
		} else {
			this.setState({
				expense: [
					{
						category: this.state.category,
						title: this.state.title,
						price: this.state.price,
					},
					...this.state.expense,
				],
			});
			this.totalExp += parseInt(this.state.price);
			this.setState({
				title: "",
				price: "",
			});
		}
	};

	deleteHandle = (index) => {
		// this.state.expense[index]
		this.totalExp -= parseInt(this.state.expense[index]["price"]);
		const newState = this.state.expense.filter(
			(item) => this.state.expense.indexOf(item) !== index
		);

		this.setState({
			expense: newState,
		});
	};

	render() {
		return (
			<div className="expense">
				<div className="expense_header">
					<form className="expense_header-input" onSubmit={this.submitHandle}>
						<select
							id="category"
							className="expense_header-input-category"
							onChange={this.handleCategory}
						>
							<option value="">Select Category</option>
							<option value="food">Food</option>
							<option value="home">Home</option>
							<option value="travel">Travel</option>
						</select>
						<Toaster position="bottom-right" reverseOrder={true} />
						<input
							type="text"
							className=""
							placeholder="Add Expense Detail"
							onChange={this.handleTitle}
							value={this.state.title}
						/>
						<input
							type="number"
							className=""
							placeholder="Amount"
							onChange={this.handlePrice}
							value={this.state.price}
						/>
						<button type="submit" className="expense_header-input-btn">
							<IoAdd />
						</button>
					</form>
				</div>

				<div className="expense__main">
					<div className="expense__main-total">
						<span>TOTAL EXPENSE</span>
						<div className="expense__main-input-price">
							<span>{this.totalExp}</span>
							<span>₹</span>
						</div>
					</div>
					<div>
						<select
							id="category"
							className="expense_main-input-category"
							onChange={this.filterCategory}
						>
							<option value="all">All</option>
							<option value="food">Food</option>
							<option value="home">Home</option>
							<option value="travel">Travel</option>
						</select>
					</div>
					{this.state.filterExp === "food"
						? this.state.expense
								.filter((exp) => exp.category === this.state.filterExp)
								.map((expense, index) => (
									<div className="expense__main-input" key={index}>
										<div className="expense__main-input-desc">
											<div className="expense__main-input-desc-cat">
												{expense.category === "home" && <BiHomeHeart />}
												{expense.category === "food" && <IoFastFoodOutline />}
												{expense.category === "travel" && <IoAirplaneOutline />}
											</div>
											<div className="expense__main-input-desc-title">
												{expense.title}
											</div>
										</div>
										<div className="expense__main-input-price">
											<span>{expense.price}</span>
											<span>₹</span>
										</div>
										<div
											className="expense__main-input-btn"
											onClick={() => this.deleteHandle(index)}
										>
											<MdDelete />
										</div>
									</div>
								))
						: this.state.filterExp === "home"
						? this.state.expense
								.filter((exp) => exp.category === this.state.filterExp)
								.map((expense, index) => (
									<div className="expense__main-input" key={index}>
										<div className="expense__main-input-desc">
											<div className="expense__main-input-desc-cat">
												{expense.category === "home" && <BiHomeHeart />}
												{expense.category === "food" && <IoFastFoodOutline />}
												{expense.category === "travel" && <IoAirplaneOutline />}
											</div>
											<div className="expense__main-input-desc-title">
												{expense.title}
											</div>
										</div>
										<div className="expense__main-input-price">
											<span>{expense.price}</span>
											<span>₹</span>
										</div>
										<div
											className="expense__main-input-btn"
											onClick={() => this.deleteHandle(index)}
										>
											<MdDelete />
										</div>
									</div>
								))
						: this.state.filterExp === "travel"
						? this.state.expense
								.filter((exp) => exp.category === this.state.filterExp)
								.map((expense, index) => (
									<div className="expense__main-input" key={index}>
										<div className="expense__main-input-desc">
											<div className="expense__main-input-desc-cat">
												{expense.category === "home" && <BiHomeHeart />}
												{expense.category === "food" && <IoFastFoodOutline />}
												{expense.category === "travel" && <IoAirplaneOutline />}
											</div>
											<div className="expense__main-input-desc-title">
												{expense.title}
											</div>
										</div>
										<div className="expense__main-input-price">
											<span>{expense.price}</span>
											<span>₹</span>
										</div>
										<div
											className="expense__main-input-btn"
											onClick={() => this.deleteHandle(index)}
										>
											<MdDelete />
										</div>
									</div>
								))
						: this.state.expense.map((expense, index) => (
								<div className="expense__main-input" key={index}>
									<div className="expense__main-input-desc">
										<div className="expense__main-input-desc-cat">
											{expense.category === "home" && <BiHomeHeart />}
											{expense.category === "food" && <IoFastFoodOutline />}
											{expense.category === "travel" && <IoAirplaneOutline />}
										</div>
										<div className="expense__main-input-desc-title">
											{expense.title}
										</div>
									</div>
									<div className="expense__main-input-price">
										<span>{expense.price}</span>
										<span>₹</span>
									</div>
									<div
										className="expense__main-input-btn"
										onClick={() => this.deleteHandle(index)}
									>
										<MdDelete />
									</div>
								</div>
						  ))}
				</div>
			</div>
		);
	}
}
